import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:project_a/providers/locele_provider.dart';

class ProjectA extends ConsumerStatefulWidget {
  const ProjectA({super.key});

  @override
  ConsumerState<ProjectA> createState() => _ProjectAState();
}

class _ProjectAState extends ConsumerState<ProjectA> {
  bool langSwitch = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Switch(
            value: ref.watch(appLocaleProvider).languageCode == 'ar'
                ? true
                : false,
            onChanged: (value) {
              langSwitch = !langSwitch;
              if (langSwitch) {
                ref
                    .read(appLocaleProvider.notifier)
                    .switchLanguage(const Locale('ar'));
              } else {
                ref
                    .read(appLocaleProvider.notifier)
                    .switchLanguage(const Locale('en'));
              }
            },
          ),
          Center(
            // child: Text("hi"),
            child: Text(AppLocalizations.of(context).helloWorld),
          ),
        ],
      ),
    );
  }
}
