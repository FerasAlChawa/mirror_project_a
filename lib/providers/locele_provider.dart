import 'dart:ui';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

final appLocaleProvider = StateNotifierProvider<AppLocale, Locale>(
  (ref) => AppLocale(
    AppLocalizations.supportedLocales.first,
  ),
);

class AppLocale extends StateNotifier<Locale> {
  AppLocale(super.state);

  bool _isSupported(Locale locale) {
    return AppLocalizations.supportedLocales.contains(locale);
  }

  void switchLanguage(Locale locale) {
    state = _isSupported(locale) ? locale : const Locale('en');
  }
}
